public class Recherche {
    public static void main(String[] args) {
        int[] tab = {4, 2, 6, 43, 7, 45};
        int[] tab1 = {2, 4, 6, 20, 22, 23, 40, 55, 66, 210};
        System.out.println(rechercheSeqNonOrdonnee(6, tab));
        System.out.println(rechercheSeqOrdonnee(40, tab1));
        System.out.println(rechercheBinaire(66, tab1));
    }

    /**
     * Effectue une fouille sequentielle dans un tableau non ordonne.
     * 
     * @param element   l'element a chercher
     * @param tab   le tableau ou chercher
     * @return  la position de la premiere occurence dans tab
     *          ou se trouve <code>element</code>, -1 si absent
     */
    private static int rechercheSeqNonOrdonnee(int element, int[] tab) {
        int i = 0;
        int reponse = -1;

        if (tab != null && tab.length > 0) {
            while(i < tab.length && tab[i] != element) {
                i++;
            }
            if (i < tab.length) {
                reponse = i;
            }
        }
        return reponse;
    }

    /**
     * Effectue une fouille sequentielle dans un tableau ordonne
     * en ordre croissant.
     * 
     * @param element   l'element a chercher
     * @param tab   le tablau ou chercher
     *              Antecedant : <code>tab</code> doit etre tie en ordre
     * @return  la position de la premier occurence dans tab ou se trouve
     *          <code>element</code>, -1 si absent.
     */
    private static int rechercheSeqOrdonnee(int element, int[] tab) {
        int i = 0;
        int reponse = -1;

        if (tab != null && tab.length > 0) {
            while (i < tab.length && tab[i] < element) {
                i++;
            }
            if (i < tab.length && tab[i] == element) {
                reponse = i;
            }
        }
        return reponse;
    }

    /**
     * Effectue une fouille binaire dans un tableau ordonne (croissant).
     * Version iterative
     * 
     * @param element   l'element a chercher
     * @param tab   le tableau ou chercher
     *              Doit etre trie en ordre croissant, non null
     * @return  La position d'une occurence dans tab ou se trouve element,
     *          -1 si absent.
     */
    private static int rechercheBinaire(int element, int[] tab) {
        int premier = 0;
        int dernier = tab.length -1;
        int milieu = -1;
        boolean trouve = false;

        while (premier <= dernier && !trouve) {
            milieu = (premier + dernier) / 2;
            if (tab[milieu] == element) {
                trouve = true;
            } else if (tab[milieu] < element) {
                premier = milieu + 1;
            } else {
                dernier = milieu - 1;
            }
        }
        if (!trouve) {
            milieu = -1;
        }
        return milieu;
    }

    /**
     * Effectue une fouille binaire dans un tableau ordonne (croissant).
     * Version recursive.
     * 
     * @param element   l'element a chercher
     * @param tab   Le tableau ou chercher
     * @return  La position d'une occurence dans tab ou se trouve element,
     *          -1 si absent.
     */
    private static int rechercheBinaireRec(int element, int[] tab, int premier, int dernier) {
        int milieu = (premier + dernier) / 2;
        if (premier <= dernier) {
            if (tab[milieu] > element) {
                milieu = rechercheBinaireRec(element, tab, premier, milieu - 1);
            } else if (tab[milieu] < element) {
                milieu = rechercheBinaireRec(element, tab, milieu + 1, dernier);
            }
        } else {
            milieu = -1;
        }
        return milieu;
    }
}
