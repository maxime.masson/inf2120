import java.util.*;

public class Tri {
    public static void main(String[] args) {
        Integer[] t = { 10, 18, 6, 15, 5, 0, 14, 11, 6, 12 };

        //triRapide(t, 0, t.length - 1);
        System.out.println(Arrays.toString(t) + "\n");
        long startTime = System.nanoTime();
        triBulle(t);
        //triSelection(t); 0 ms
        //triRapide(t, 0, t.length - 1); 41ms
        long endTime = System.nanoTime();

        System.out.println((endTime - startTime) / 1000000);

        // Arrays.sort(t, (e1, e2) -> e1.compareTo( e2 ));
        // Arrays.sort(t, integer::compareTo);

        // Arrays.stream(t).forEach(System.out::println);
    }

	public static < E extends Comparable< E > > void triSelection( E [] tab ) {
		triSelection( tab, 0, tab.length - 1 );
    }
    public static <E extends Comparable<E>> void triSelection(E[] tab, int debut, int fin) {
        for (int i = debut; i < fin; i++) {
            int position = i;
            E minima = tab[i];

            for (int j = i + 1; j <= fin; j++) {
                if (tab[j].compareTo(minima) < 0) {
                    position = j;
                    minima = tab[j];
                }
            }
            tab[position] = tab[i];
            tab[i] = minima;
            System.out.println(Arrays.toString(tab));
        }
    }


    public static <E extends Comparable<E>> void triInsertion(E[] tab) {
        for (int i = 1; i < tab.length; i++) {
            E temp = tab[i];
            int j = i - 1;
            while (0 <= j && temp.compareTo(tab[j]) < 0) {
                tab[j + 1] = tab[j];
                j--;
            }
            tab[j + 1] = temp;
        }
        System.out.println(Arrays.toString(tab));
    }

    public static <E extends Comparable<E>> void triBulle(E[] tab) {
        for (int i = 0; i < tab.length; i++) {
            System.out.println("Boucle de tri numéro " + i + " :");
            for (int j = 0; j < tab.length - 1; j++) {
                if (tab[j].compareTo(tab[j + 1]) > 0) {
                    E temp = tab[j];
                    tab[j] = tab[j + 1];
                    tab[j + 1] = temp;
                }
            System.out.println("Boucle j numéro " + j + " :" );
            System.out.println(Arrays.toString(tab));
            }
        }
    }

    public static <E extends Comparable<E>> void triRapide(E[] tab, int debut, int fin) {
        if (debut<fin) {
            E pivot = tab[(debut + fin) / 2];
            int i = debut - 1;
            int j = fin + 1;
            
            while ( i < j) {
                do j--; while(tab[j].compareTo(pivot) > 0);
                do i++; while(tab[i].compareTo(pivot) < 0);
                if (i<j) {
                    E temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                }
                System.out.println("i = " + i + " et j = " + j);
                System.out.println("Pivot = " + pivot);
                System.out.println(Arrays.toString(tab) + "\n");
            }
            triRapide(tab, debut, j);
            triRapide(tab, j + 1, fin);
        }
    }

}
