public class ArbreBinaireRecherche <T extends Comparable<T>> implements TdaAbr<T> {
    private static boolean estInsere;
    private static boolean estRetire;

    private Noeud<T> racine;

    public ArbreBinaireRecherche() {
        racine = null; // facultatif
    }
    
    /**
     * Construit un ABR a partir d'un noeud.
     * @param element La cle (valeur) de la racine.
     */
    public ArbreBinaireRecherche(T element) {
        racine = new Noeud(element);
    }

    @Override
    public boolean estVide() {
        return racine == null;
    }
    @Override
    public int nbNoeuds() {
        return nbNoeuds(racine);
    }
    @Override
    public boolean estPresent(T element) {
        return chercher(racine, element) != null;
    }
    @Override
    public Noeud<T> chercher(T element) {
        return chercher(racine, element);
    }

    /**
     * Cherche element dans cet ABR(version iterative alternative).
     * @param element L'element a chercher
     * @return La reference au noeud contenant <code>element</code> si present,
     *          null sinon.
     */
    public Noeud<T> chercherIte(T element) {
        return chercherIte(racine, element);
    }

    @Override
    public boolean inserer(T element) {
        estInsere = true;
        racine = inserer(racine, element);
        return estInsere;
    }

    public boolean insererIte(T element) {
        estInsere = true;
        racine = insererIte(racine, element);
        return estInsere;
    }

    @Override
    public boolean retirer(T element) {
        estRetire = false;
        racine = retirer(racine, null, element); // La racine peut etre modifie
        return estRetire;
    }

    private Noeud<T> chercher(Noeud<T> n, T element) {
        Noeud<T> reponse;
        int direction;
        if (n ==  null || element == null) {
            reponse = null;
        } else {
            direction = element.compareTo(n.info());
            if (direction == 0) {
                reponse = n;
            } else if (direction < 0) {
                reponse = chercher(n.gauche(), element);
            } else {
                reponse = chercher(n.droite(), element);
            }
        }
        return reponse;
    }

    /**
     * Permet d'obtenir le noeud contenant l'element donne s'il est
     * present dans l'ABR dont la racine est n.
     * @param n la racine de l'ABR dans lequel chercher <code>element</code>.
     * @param element L'element a chercher.
     * @return le noeud contenant l'element donne s'il est present dans
     *          l'ABR dont la racine est n ou null si l'element n'est pas
     *          present.
     */
    private Noeud<T> chercherIte(Noeud<T> n, T element) {
        boolean trouve = false;
        Noeud<T> p = n;
        int direction;
        while(!trouve && p != null) {
            direction = element.compareTo(p.info());
            if (direction == 0) {
                trouve = true;
            } else if (direction < 0) {
                p = p.gauche();
            } else {
                p = p.droite();
            }
        }
        return p;
    }

    /**
     * Insere l'element donne dans l'ABR dont la racine est n.
     * @param n la racine de l'ABR dans lequel on veut
     *          inserer l'element donne.
     * @param element   l'element a inserer.
     * @return  la racine de cet ABR (peut avoir ete modifiee).
     */
    private Noeud<T> inserer(Noeud<T> n, T element) {
        int direction;
        if (n == null) {
            n = new Noeud<>(element);
        } else {
            direction = element.compareTo(n.info());
            if (direction == 0) {
                estInsere = false; //l'element y est deja
            } else if (direction < 0) {
                n.modifierGauche(inserer(n.gauche(), element));
            } else {
                n.modifierDroite(inserer(n.droite(), element));
            }
        }
        return n;
    }

    /**
     * Insere l'element donne dans l'ABR dont la racine est n.
     * @param n la racine de l'ABR dans lequel on veut inserer
     *          l'element donne.
     * @param element   l'element a inserer.
     * @return  la racine de cet ABR (peut avoir ete modifiee).
     */
    private Noeud<T> insererIte(Noeud<T> n, T element) {
        Noeud<T> p = n;
        int direction;
        boolean endroitTrouve = false;
        Noeud<T> reponse;
        if (n == null) {
            reponse = new Noeud(element);
        } else {
            while(!endroitTrouve) {
                direction = element.compareTo(p.info());
                if (direction == 0) {
                    endroitTrouve = true;
                    estInsere = false;
                } else if (direction < 0) {
                    if (p.gauche() == null) {
                        endroitTrouve = true;
                        p.modifierGauche(new Noeud(element));
                    } else {
                        p = p.gauche();
                    }
                } else {
                    if (p.droite() == null) {
                        endroitTrouve = true;
                        p.modifierDroite(new Noeud(element));
                    } else {
                        p = p.droite();
                    }
                }
            }
            reponse = n;
        }
        return reponse;
    }

    /**
     * Retourne le nombre de noeuds dans l'ABR dont la racine est celle
     * donnee en parametre.
     * @param racine    La racine de l'ABR dont on veut le nombre de
     *                  noeuds
     * @return  Le nombre de noeuds de l'ABR dont la racine est celle
     *          donnee en parametre.
     */
    private int nbNoeuds(Noeud<T> racine) {
        int nombre = 0;
        if (racine != null) {
            nombre = 1 + nbNoeuds(racine.gauche()) + nbNoeuds(racine.droite());
        }
        return nombre;
    }

    /**
     * Retire l'element donne dans l'ABR dont la racine est n, s'il est
     * present.
     * @param n la racine de l'ABR dans lequel chercher element.
     * @param parent    le noeud parent de n.
     * @param element   l'element a retirer.
     * @return  la racine de cet ABR (peut avoir ete modifiee).
     */
    private Noeud<T> retirer(Noeud<T> n, Noeud<T> parent, T element) {
        Noeud<T> reponse = racine;
        int direction;

        if (n != null && element != null) {
            //chercher le noeud contenant element
            direction = element.compareTo(n.info());

            //on a trouve le noeud contenant element
            if (direction == 0) {
                estRetire = true; // element existe donc il sera retire

                // le noeud a retirer est une feuille
                if (n.gauche() == null && n.droite() == null) {
                    reponse = retirerFeuille(n, parent);

                // le noeud a retirer a seulement un fils gauche ou un fils droit
                } else if (n.gauche() == null || n.droite() == null) {
                    reponse = retirerNoeudAvecUnFils(n, parent);

                // le noeud a retirer a deux fils
                } else {
                    retirerNoeudAvecDeuxFils(n, n.gauche(), n);
                }
            } else if (direction < 0) { // chercher a gauche
                reponse = retirer(n.gauche(), n, element);
            } else {                    // chercher a droite
                reponse = retirer(n.droite(), n, element);
            }
        }
        return reponse;
    }

    /**
     * Retire le noeud de cet ABR.
     * @param n le noeud a retirer. ANT : n est une feuille (non null).
     * @param parent    le noeud parent de n.
     * @return  la racine de cet ABR (peut avoir ete modifiee).
     */
    private Noeud<T> retirerFeuille(Noeud<T> n, Noeud<T> parent) {
        if (n == racine) {
            racine = null;
        } else if (parent.gauche() == null) {
            parent.modifierGauche(null);
        } else {
            parent.modifierDroite(null);
        }
        return racine;
    }

    /**
     * Retire le noeud n de cet ABR.
     * @param n le noeud a retirer. ANT : n est non null et n'a qu'un
     *          seul fils.
     * @param parent    le noeud parent de n.
     * @return  La racine de cet ABR (peut avoir ete modifiee).
     */
    private Noeud<T> retirerNoeudAvecUnFils(Noeud<T> n, Noeud<T> parent) {

        // determiner si le fils de n est un fils droit ou gauche
        Noeud<T> fils = n.droite();
        if (n.gauche() != null) {
            fils = n.gauche();
        }

        // n est la racine : modifier racine par le fils de n
        if (n == racine) {
            racine = fils;

        // n est le fils gauche de parent : remplacer le fils gauche de
        // parent par le fils de n.
        } else if (parent.gauche() == n) {
            parent.modifierGauche(fils);

        // n est le fils droit de parent : remplacer le fils droit de
        // parent par le fils de n
        } else {
            parent.modifierDroite(fils);
        }
        return racine;
    }

    /**
     * Retire le noeud n de cet ABR, en le remplacant par son predecesseur
     * (le maximum dans son sous-arbre gauche).
     * @param n le noeud a supprimer. ANT : n est non null et a deux fils.
     * @param max   le maximum dans le sous-arbre gauche de n
     *              (initialise a n.gauche au premier appel).
     * @param parentMax le parent de max (initialise a n au premier appel).
     * @return  la racine de cet ABR.
     */
    private void retirerNoeudAvecDeuxFils(Noeud<T> n, Noeud<T> max, Noeud<T> parentMax) {

        // chercher max : aller vers la droite jusqu'a trouver le max;
        if (max.droite() != null) {
            retirerNoeudAvecDeuxFils(n, max.droite(), max);

        // max trouve
        } else {
            //copier info de max dans n
            n.modifierInfo(max.info());

            // supprimer max qui est une feuille
            if (max.droite() == null && max.gauche() == null) {
                retirerFeuille(max, parentMax);

            // supprimer max qui a un seul fils gauche
            } else {
                retirerNoeudAvecUnFils(max, parentMax);
            }
        }
    }

    public static int hauteur(Noeud racine) {
        int reponse = -1;
        if (racine != null) {
            reponse = 1 + Math.max(hauteur(racine.gauche()), hauteur(racine.droite()));
        }
        return reponse;
    }

    public static Integer maximum (Noeud<Integer> racine) {
        Integer max = null;
        if (racine != null) {
            if (racine.droite() == null) {
                max = racine.info();
            } else {
                max = maximum(racine.droite());
            }
        }
        return max;
    }

}
