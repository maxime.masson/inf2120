public class Main {
    public static void main(String[] args) {
        TdaAbr<String> abr = new ArbreBinaireRecherche<String>();
        boolean b;
        Noeud<String> n;
        int nbr;

        b = abr.estVide();
        abr.inserer("coucou");
        abr.inserer("salut");
        abr.inserer("adios");
        abr.inserer("ciao");
        abr.inserer("tata");

        b = abr.estPresent("adios"); // b = true
        System.out.println("B devrait être true... B est " + b);
        b = abr.estVide();           // b = false
        System.out.println("B devrait etre false... B est " + b);

        n = abr.chercher("A+"); // n est null
        System.out.println("N devrait être null... N est " + n);
        n = abr.chercher("ciao");// n non null
        System.out.println("N devrait être non null... N est " + n);
        nbr = abr.nbNoeuds();   // nbr = 5
        System.out.println("Nombre devrait être 5... Nombre est " + nbr);
        b = abr.retirer("adios");// true
        System.out.println("B devrait être true... B est " + b);
        nbr = abr.nbNoeuds();   // nbr = 4
        System.out.println("Nombre devrait être 4... Nombre est " + nbr);
    }
}