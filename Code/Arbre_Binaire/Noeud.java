public class Noeud <T extends Comparable> {
    private T info; // la clef/valeur de ce noeuf
    private Noeud<T> gauche; // Enfant gauche du noeud
    private Noeud<T> droite; // Enfant droite du noeud

    public Noeud(T element) {
        info = element;
    }

    public T info() {
        return info;
    }

    public Noeud<T> gauche() {
        return gauche;
    }

    public Noeud<T> droite() {
        return droite;
    }

    public void modifierInfo(T element) {
        info = element;
    }

    public void modifierGauche(Noeud<T> gauche) {
        this.gauche = gauche;
    }

    public void modifierDroite(Noeud<T> droite) {
        this.droite = droite;
    }

    public String toString() {
        return this.info + "";
    }
}