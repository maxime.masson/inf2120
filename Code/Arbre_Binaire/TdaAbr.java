public interface TdaAbr<T extends Comparable<T>> {
    public abstract boolean estVide();
    public abstract int nbNoeuds();
    public abstract boolean estPresent(T element);
    public abstract Noeud<T> chercher(T element);
    public abstract boolean inserer(T element);
    public abstract boolean retirer(T element);
}