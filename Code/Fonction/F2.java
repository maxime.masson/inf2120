public class F2 implements Fonction<Integer, Integer> {
    @Override
    public Integer appliquer(Integer x) {
        return 4 * x * x - 7;
    }
}