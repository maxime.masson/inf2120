import java.util.function.Consumer;

public class Somme implements Consumer<Integer> {
    protected Integer s = 0;

    @Override
    public void accept(Integer x) {
        this.s = this.s + x;
    }

    public Integer lireS() {
        return s;
    }
}