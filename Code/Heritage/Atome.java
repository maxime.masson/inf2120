public enum Atome {	
	HYDROGENE( "H",  "Hydrog&egrave;ne",        1,  1, 1, Famille.NON_METAUX,               1.00794 ),
	HELIUM   ( "He", "H&eacute;lium",           2, 18, 1, Famille.GAZ_NOBLE,                4.002602 ),
	LITHIUM  ( "Li", "Lithium",                 3,  1, 2, Famille.METAL_ALCALIN,            6.941 ),
	BERYLLIUM( "Be", "B&eacute;ryllium",        4,  2, 2, Famille.METAL_ALCALINO_TERREUX,   9.0121831 ),
	BORE     ( "B",  "Bore",                    5, 13, 2, Famille.METALLOIDE,              10.811 ),
	CARBONE  ( "C",  "Carbone",                 6, 14, 2, Famille.NON_METAUX,              12.01074 ),
	AZOTE    ( "N",  "Azote",                   7, 15, 2, Famille.NON_METAUX,              14.0067 ),
	OXIGENE  ( "O",  "Oxyg&egrave;ne",          8, 16, 2, Famille.NON_METAUX,              15.9994 ),
	FLUOR    ( "F",  "Fluor",                   9, 17, 2, Famille.HALOGENE,                18.998403163 ),
	NEON     ( "Ne", "N&eacute;on",            10, 18, 2, Famille.GAZ_NOBLE,               20.1797 ),
	SODIUM   ( "Na", "Sodium",                 11,  1, 3, Famille.METAL_ALCALIN,           22.98976928 ),
	MAGNESIUM( "Mg", "Magn&eacute;sium",       12,  2, 3, Famille.METAL_ALCALINO_TERREUX,  24.3050 ),
	ALUMINIUM( "Al", "Aliminium",              13, 13, 3, Famille.METAL_PAUVRE,            26.9815386 ),
	SILICIUM ( "Si", "Silicium",               14, 14, 3, Famille.METALLOIDE,              28.0855 ),
	PHOSPHORE( "P",  "Phosphore",              15, 15, 3, Famille.NON_METAUX,              30.973761998 ),
	SOUFRE   ( "S",  "Soufre",                 16, 16, 3, Famille.NON_METAUX,              32.065 ),
	CHLORE   ( "Cl", "Chlore",                 17, 17, 3, Famille.HALOGENE,                35.453 ),
	ARGON    ( "Ar", "Argon",                  18, 18, 3, Famille.GAZ_NOBLE,               39.948 ),
	POTASSIUM( "K",  "Potassium",              19,  1, 4, Famille.METAL_ALCALIN,           39.0983 ),
	CALCIUM  ( "Ca", "Calcium",                20,  2, 4, Famille.METAL_ALCALINO_TERREUX,  40.078 ),
	SCANDIUM ( "Sc", "Scandium",               21,  3, 4, Famille.METAL_DE_TRANSITION,     44.955908 ),
	TITANE   ( "Ti", "Titane",                 22,  4, 4, Famille.METAL_DE_TRANSITION,     47.867 ),
	VANADIUM ( "V",  "Vanadium",               23,  5, 4, Famille.METAL_DE_TRANSITION,     50.9415 ),
	CHROME   ( "Cr", "Chrome",                 24,  6, 4, Famille.METAL_DE_TRANSITION,     51.9961 ),
	MANGANESE( "Mn", "Mangan&egrave;se",       25,  7, 4, Famille.METAL_DE_TRANSITION,     54.938044 ),
	FER      ( "Fe", "Fer",                    26,  8, 4, Famille.METAL_DE_TRANSITION,     55.845 ),
	COBALT   ( "Co", "Cobalt",                 27,  9, 4, Famille.METAL_DE_TRANSITION,     58.933194 ),
	NICKEL   ( "Ni", "Nickel",                 28, 10, 4, Famille.METAL_DE_TRANSITION,     58.6934 ),
	CUIVRE   ( "Cu", "Cuivre",                 29, 11, 4, Famille.METAL_DE_TRANSITION,     63.546 ),
	ZINC     ( "Zn", "Zinc",                   30, 12, 4, Famille.METAL_PAUVRE,            65.409 ),
	GALLIUM  ( "Ga", "Gallium",                31, 13, 4, Famille.METAL_PAUVRE,            69.723 ),
	GERMANIUM( "Ge", "Germanium",              32, 14, 4, Famille.METALLOIDE,              72.64 ),
	ARSENIC  ( "As", "Arsenic",                33, 15, 4, Famille.METALLOIDE,              74.921595 ),
	SELENIUM ( "Se", "S&eacute;l&eacute;nium", 34, 16, 4, Famille.NON_METAUX,              78.971 ),
	BROME    ( "Br", "Brome",                  35, 17, 4, Famille.HALOGENE,                79.904 ),
	KRYPTON  ( "Kr", "Krypton",                36, 18, 4, Famille.GAZ_NOBLE,               83.798 );
	
	
	private String  _symbole;
	private String  _nom;
	private int     _noAtomique;
	private int     _groupe;
	private int     _periode;
	private Famille _famille;
	private double  _masseAtomique;
	
	private Atome( String symbole, String nom, 
			       int noAtomique, int groupe,
			       int periode, Famille famille, 
			       double masseAtomique ) {
		_symbole = symbole;
		_nom = nom;
		_noAtomique = noAtomique;
		_groupe = groupe;
		_periode = periode;
		_famille = famille;
		_masseAtomique = masseAtomique;		
	}
	
	public String symbole() {
		return _symbole;
	}
	public String nom() {
		return _nom;
	}
	public int noAtomique() {
		return _noAtomique;
	}
	public int groupe() {
		return _groupe;
	}
	public int periode() {
		return _periode;
	}
	public Famille famille() {
		return _famille;
	}
	public double masseAtomique() {
		return _masseAtomique;
	}
}