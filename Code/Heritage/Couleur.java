public enum Couleur {
	BLANC, NOIR;
	
	public String chaineReduite() {
		return this == BLANC ? "B" : "N";
	}
}