public class DeplacementInvalide extends Exception {
	private static final long serialVersionUID = 1L;

	public DeplacementInvalide() {
		super();
	}
	
	public DeplacementInvalide( String mssg ) {
		super( mssg );
	}
}