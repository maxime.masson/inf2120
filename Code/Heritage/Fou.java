public class Fou extends PieceEchec {
	public Fou( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur) {
		super( echiquier, ligne, colonne, couleur );
	}
	
	@Override
	public boolean deplacementValide( int ligne, Colonne colonne ) {
		int deltaColonne = Math.abs( this.colonne.ordinal() - colonne.ordinal() );
		int deltaLigne = Math.abs( this.ligne - ligne );

		return ( deltaColonne != 0 ) && ( deltaColonne == deltaLigne );
	}
	
	@Override
	public String chaineReduite() {
		return "F" + couleur.chaineReduite();
	}

	@Override
	public String toString() {
		return "Fou" + super.toString();
	}
}