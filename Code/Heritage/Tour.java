public class Tour extends PieceEchec {
	public Tour( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur) {
		super( echiquier, ligne, colonne, couleur );
	}
	
	@Override
	public boolean deplacementValide( int ligne, Colonne colonne ) {
		return ( this.ligne == ligne ) ^ ( this.colonne == colonne );
	}
	
	@Override
	public String chaineReduite() {
		return "T" + couleur.chaineReduite();
	}

	@Override
	public String toString() {
		return "Tour" + super.toString();
	}
}