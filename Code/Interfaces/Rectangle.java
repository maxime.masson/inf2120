public class Rectangle implements Forme2D {
	protected double base;
	protected double hauteur;
	
	public Rectangle( double b, double h ) {
		base = b;
		hauteur = h;
	}

	@Override
	public double aire() {
		return base * hauteur;
	}
}