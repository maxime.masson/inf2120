import java.util.List;
import java.util.stream.Stream;

public class Principal {
    public static void main(String[] args) {
        List<Integer> a = List.of(3, 5, 7, 2, 4, 6, 89, 0, 5, 3, 5, 7);

        // Stream<Integer> s1 = a.stream();
        // Stream<Integer> s2 = s1.map(x -> 2 * x);
        // Stream<Integer> s3 = s2.filter(x -> x < 10);
        // Stream<Integer> s4 = s3.map(x -> x - 1);
        // s4.forEach(x -> System.out.println(x));

        a.stream().map(x -> 2 * x).filter(x -> x < 10).map(x -> x - 1 ).forEach(x -> System.out.println(x));
        // OR
        a.stream().map(x -> 2 * x).filter(x -> x < 10).map(x -> x - 1 ).forEach(System.out::println);
        
    }
}