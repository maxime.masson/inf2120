public class ExceptionPileIsEmpty extends Exception {
    public ExceptionPileIsEmpty() {
        super();
    }

    public ExceptionPileIsEmpty(String mssg) {
        super(mssg);
    }
}