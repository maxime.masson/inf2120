class FactoriellePile {
    public static int factorielle(int n) {
        Pile<Integer> pile = new PileArrayList<Integer>();
        int reponse;
        if (n < 0) {
            reponse = 0;
        } else {
            // empiler les infos while ( n > 1 ) {
            pile.empiler(n);
            n = n - 1; // simuler l’appel de factorielle ( n - 1 ) }
            // cas de base
            reponse = 1; // contient la valeur de factorielle ( n - 1 ) while ( !pile.estVide() ) {
            n = pile.depiler();
            reponse = reponse * n; // simuler n * factorielle ( n - 1 ) }
        }
        return reponse;
    }
}