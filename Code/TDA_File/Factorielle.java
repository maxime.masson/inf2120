class Factorielle {
    public static int factorielle(int n) {
        int resultat;
        if (n < 2)
            resultat = 1;
        else
            resultat = n * factorielle(n - 1);
        return resultat;
    }

    public static void main(String[] args) {

        try {
            int x = factorielle(Integer.parseInt(args[0]));
            System.out.println(x);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Veuillez entrer un seul argument afin de calculer sa factorielle");
        }

    }
}