class FactorielleIteratif {
    public static int factorielle(int n) {
        int reponse = 0;
        if (n < 0) {
            reponse = 0; // erreur retourne 0 } else {
            reponse = 1;
            for (int i = 2; i <= n; i++) {
                reponse = reponse * i;
            }
        }
        return reponse;
    }
}