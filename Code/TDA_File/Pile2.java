public class Pile2<E> {
    protected class Link<T> {
        protected E element;
        protected Link<T> previous;

        public Link(E element) {
            this.element = element;
            previous = null;
        }

        public Link(E element, Link<T> previous) {
            this.element = element;
            this.previous = previous;
        }

        public getElement() {
            return this.element;
        }

        public setPrevious() {

        }

        public getPrevious() {

        }
    }

    protected Link<E> top;
    protected int size;

    public Pile2() {
        top = null;
    }

    public boolean isEmpty() {
        return null == top;
    }

    public E getTop() throws ExceptionPileIsEmpty {
        if (null == top) {
            throw new ExceptionPileIsEmpty("getSomme");
        }
        return top.getElement();
    }

    public void push(E element) {
        top = new Link(element, top);
        size++;
    }

    public E pop() throws ExceptionPileIsEmpty {
        E value = getTop(); // Method is already testing if the stack is empty
        top = top.getPrevious();
        size--;
        return value;
    }

    public int getSize() {
        return size;
    }
    public int size() {
        // Iterate through the stack
        int s = 0;
        Link<E> current = top;
        while (null != current) {
            current = current.getPrevious();
            s++;
        }
        return s;
    }
}