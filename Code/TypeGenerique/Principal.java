import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class Principal {
	public static void echanger( int [] tab, int i, int j ) {
		int temp = tab[i];
		tab[i] = tab[j];
		tab[j] = temp;
	}
	public static void echanger( double [] tab, int i, int j ) {
		double temp = tab[i];
		tab[i] = tab[j];
		tab[j] = temp;
	}
	
	public static <E> void echanger( E [] tab, int i, int j ) {
		E temp = tab[i];
		tab[i] = tab[j];
		tab[j] = temp;
	}
	
	public static <T, U> void aff1( T t, U u, boolean gauche) {
		// instruction 'if'
		if( gauche ) {
			System.out.println( t );
		} else {
			System.out.println( u );
		}
		// operateur ternaire.
		// expression 'if'
		System.out.println( gauche ? t : u );	
	}


	
	public static Pair< Integer, Double > avantMoitier( int n ) {
		//return n-1, n+1 ;
		return new Pair<Integer, Double>( n - 1, ((double)n) / 2.0 );
	}
	
	public static void main( String [] args ) {
		Pair<Integer, Double> reponse = avantMoitier( 4 );
		
		Pair r2 = new Pair<>("allo",4);
		
		r2.premier = "b";
		
		reponse.premier = 2;
		reponse.premier = reponse.premier + 12;
		
		System.out.println( reponse.premier );
		System.out.println( reponse.deuxieme );
	}
}