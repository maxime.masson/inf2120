import java.util.*;
import java.lang.*;

public class Cercle extends Forme2D {
    private double rayon;

    public Cercle(double r) {
        this.rayon = r;
    }

    @Override
    public double calculerAire() {
        return Math.PI * Math.pow(rayon,2);
    }

    public String toString() {
        return "Le rayon du cercle est " + rayon;
    }
}
