public class Rectangle extends Forme2D {
    private double hauteur;
    private double largeur;

    public Rectangle(double h, double l) {
        this.hauteur = h;
        this.largeur = l;
    }

    @Override
    public double calculerAire() {
        return hauteur * largeur;
    }

    public String toString() {
        return "Le rectangle a une largeur de " + largeur + " et une hauteur de " + hauteur + ".";
    }
}
