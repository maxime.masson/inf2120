
import java.util.ArrayList;

public class Principal {
	public static <T> PeutEtre<Integer> trouverElement( T[] a_tableau, T a_element ) {
		PeutEtre<Integer> resultat = null;
		
		int i = 0;
		while( i < a_tableau.length && ( ! a_element.equals( a_tableau[i] ) ) ) {
			++ i;
		}
		if( i < a_tableau.length ) {
			resultat = new QQChose<Integer>( new Integer( i ) );
		} else {
			resultat = new Rien<Integer>();
		}
		
		return resultat;
	}
	
	public static ArrayList<Double> tweens( double depart, double fin, int nbrInterval ){
		ArrayList<Double> resultat = new ArrayList<>();
		
		double distance = fin - depart;
		double delta = distance / (double)nbrInterval;
		
		for( int i = 0; i <= nbrInterval; ++ i ) {
			resultat.add( new Double( depart ) );
			depart += delta;
		}
		
		return resultat;
	}
	
	public static void main( String [] argv ) {
		// Integer [] tab = { new Integer( 9 ), new Integer( 5 ), new Integer ( 6 ) };
		
		// PeutEtre<Integer> pos = trouverElement( tab, new Integer(5) );
		// if( pos.estQQChose() ) {
		// 	try {
		// 		System.out.println( pos.qQChose() );
		// 	} catch( ARien e ) {
		// 		System.out.println( "erreur, cette exception ne devrait pas etre lance." );
		// 	}
		// } else {
		// 	System.out.println( "erreur, aurait du trouver QQChose." );
		// }
		
		// PeutEtre<Integer> pos2 = trouverElement( tab, new Integer(1) );
		// if( pos2.estRien() ) {
		// 	System.out.println( "correct, rien trouve." );
		// } else {
		// 	System.out.println( "erreur, aurait du trouver Rien." );
		// }
		
		ArrayList<Double> r = tweens( 1.0, 3.0, 4 );
		r.forEach( System.out::println );
	}
}
