package Demo3;
public class QQChose<T> extends PeutEtre<T> {
	private T valeur;
	
	public QQChose( T valeur ){
		this . valeur = valeur;
	}

	@Override
	public boolean estQQChose() {
		return true;
	}

	@Override
	public boolean estRien() {
		return false;
	}

	@Override
	public T qQChose() throws ARien {
		return valeur;
	}
}
