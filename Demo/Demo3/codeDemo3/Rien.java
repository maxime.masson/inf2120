package Demo3;
public class Rien<T> extends PeutEtre<T> {
	public Rien(){
	}
	
	@Override
	public boolean estQQChose() {
		return false;
	}

	@Override
	public boolean estRien() {
		return true;
	}

	@Override
	public T qQChose() throws ARien {
		throw new ARien();
	}

}
