/** La classe ARien hérite de la classe Exception (Classe de tous les exceptions) */
/**Constructeur ARien crée un objet selon les propriétés de Exception*/

public class ARien extends Exception {
    public ARien() {
        super();
    }

    public ARien(String message) {
        super(message);
    }
}
