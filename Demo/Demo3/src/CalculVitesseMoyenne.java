public class CalculVitesseMoyenne {

    public static void main (String[] args) {

        // Variables
        int nbTroncon = 0;
        double nbKiloRecu;
        double vitRecu;
        double nbKilo = 0;
        double vitMoyenne = 0;
        double vitMoyenneTotal = 0;
        double nbHeures =0;
        char oui = 'o';
        char non = 'n';
        char ouiNon;

        // Constantes
        String MSG_DEBUT = "Ce programme calcule la vitesse moyenne pour un trajet effectue en plusieurs troncons.\nAppuer sur [ENTREE] pour continuer.";
        String MSG_KILO = "Entrez le nombre de kilometre(s) pour le troncon #" + nbTroncon + " :";
        String MSG_VITESSE = "Entrez la vitesse (km/h) pour le troncon #" + nbTroncon + " :";
        String MSG_ERREUR = "Erreur. Le nombre de kilometre(s) doit etre superieur ou egal a 0.";
        String MSG_FIN = "Fin normale du programme";
        String MSG_CONTINUER = "Voulez-vous comptabiliser un autre troncon? (o/n):";


        System.out.println(MSG_DEBUT);
        Clavier.lireFinLigne();

        do {

            if (nbTroncon >= 0) {
                nbTroncon++;
            }

            System.out.println("------------");
            System.out.println("TRONCON #" + nbTroncon);
            System.out.println("------------");


            System.out.println(MSG_KILO);
            nbKiloRecu = Clavier.lireIntLn();

            while (nbKiloRecu <= 0) {
                System.out.println(MSG_ERREUR);
                System.out.println(MSG_KILO);
                nbKiloRecu = Clavier.lireIntLn();
            }

            if (nbKiloRecu > 0){
                nbKilo += nbKiloRecu;
            }

            System.out.println(MSG_VITESSE);
            vitRecu = Clavier.lireIntLn();

            while (vitRecu <= 0) {
                System.out.println(MSG_ERREUR);
                System.out.println(MSG_VITESSE);
                vitRecu = Clavier.lireIntLn();
            }

            if (vitRecu > 0) {
                vitMoyenne += vitRecu;
                nbHeures += nbKiloRecu / vitRecu;
            }

            System.out.println(MSG_CONTINUER);
            ouiNon = Clavier.lireCharLn();


        } while (ouiNon == oui);


        if (ouiNon == non) {

            vitMoyenneTotal = nbKilo / nbHeures;
            System.out.println("La vitesse moyenne est: " + Math.round(vitMoyenneTotal * 100.0) / 100.0 + " km/h. \n");

        }

        System.out.println(MSG_FIN);
    }

}