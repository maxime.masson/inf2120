/** Classe Abstraite avec méthode abstraite.
 * On pourra y faire appel dans une autre classe en déterminant ce qu'elle retourne. La méthode abstraite ne retourne rien
 * Type générique T (object)
 * @param <T>
 */
public abstract class PeutEtre <T> {
    public abstract boolean estQQChose();
    public abstract boolean estRien();
    public abstract T qQChose() throws ARien;

}
