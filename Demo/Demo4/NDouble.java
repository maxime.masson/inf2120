public class NDouble implements Nombre<NDouble> {
    private double valeur;

    public NDouble(double valeur) {
        this.valeur = valeur;
    }

    public double getValeur() {
        return valeur;
    }
    @Override
    public NDouble add(NDouble x) {
        return new NDouble(valeur + x.valeur);
    }
    @Override
    public NDouble sub(NDouble x) {
        return new NDouble(valeur - x.valeur);
    }
    @Override
    public NDouble mul(NDouble x) {
        return new NDouble(valeur * x.valeur);
    }
    @Override
    public NDouble div(NDouble x) {
        return new NDouble(valeur / x.valeur);
    }

    public String toString() {
        return "Le nombre est: " + valeur;
    }
    
}