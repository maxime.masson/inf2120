public interface Nombre<N> {
    N add(N x);
    N sub(N x);
    N mul(N x);
    N div(N x);

    // Cette interface décrit les différentes fonctions entre deux nombres,
    // soit Addition, Soustraction, Multiplication et division
    
}