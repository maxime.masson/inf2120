import java.util.*;
class Principal {

    public static void main(String[] params) {
        Fraction x = new Fraction(2,3);
        Fraction y = new Fraction(1,2);
        NDouble xD = new NDouble(10);
        NDouble yD = new NDouble(5);

        NDouble zD = xD.add(yD);
        System.out.println(zD); // retourne 10 + 5

        Fraction z = x.mul(y);
        System.out.println(z); // Retourne 2/3 * 1/2 = 1/3

        ArrayList<Fraction> Arr1 = new ArrayList<>(Arrays.asList(new Fraction(1,2), new Fraction(1,2), new Fraction(1,2)));
        ArrayList<NDouble> Arr2 = new ArrayList<>(Arrays.asList(new NDouble(10), new NDouble(5), new NDouble(7)));

        System.out.println(somme(Arr1)); // Retourne 1.5
        System.out.println(somme(Arr2)); // Retourn 22
    }

    public static < N extends Nombre<N> > Nombre<N> somme(ArrayList<N> tableau) {

        Nombre<N> somme = null;
        if (!tableau.isEmpty()) {
            somme = tableau.get(0);

            for (int i = 1; i<tableau.size(); i++) {
                somme = somme.add(tableau.get(i));
            }
        }
        return somme;
    }
}