---
title : Cours 13 - JAVA II
author: [Maxime Masson]
titlepage : true
---

# Resume

- TRI RAPIDE (Quicksort)


# Arbre Binaire
---

Nous appellons chaque marqueur d'élément une `clé`.
On *extends* Comparable car nous devrons comparer chaque objet afin de déterminer vers quelle direction nous créerons le prochain maillon.

***Note*** : Voir classe ci-dessous `pair` pour comprendre la méthode `chercherParentCourant()`.

```java

/**
 * Code définissant un Arbre Binaire
 * Nécessite trois méthodes spécifiques - Chercher, Insérer, Supprimer.
 * Voir TreeMap<K,V> dans la Doc
 * @param <K>
 */
public class ABR< K extends Comparable<K> > {
    private class Noeud< K  extends Comparable<K2>> {
        public K2 clef;
        public Noeud< K2 > gauche;
        public Noeud< K2 > droite;

        public Noeud( K2 clef ) {
            this.clef = clef;
        }
    }

    protected Noeud<K> racine;

    // Si arbre vide, null = contenue du constructeur.
    public ABR() {}

    // Méthode 1 - Insérer une clef dans l'arbre
    public void inserer(K clef) {

    }

    // Méthode 2 - Supprimer une clef de l'arbre
    public void supprimer(K clef) {

    }

    // Méthode 3 - Chercher une clef et retourner True/False si elle est ou non présente
    public boolean chercher(K clef) {
        Noeud<K> resultat = null == racine ? null : racine.chercher(clef);
        return null != resultat;
    }
    
    public Noeud<K2> chercher(K2 clef) {
        Noeud<K2> resultat = this;
        int direction = clef.compareTo(this.clef);

        if (direction != 0) {
            if (direction < 0) {
                if (null != gauche) {
                    resultat = gauche.chercher(clef);
                } else {
                    resultat = null;
                }
            } else {
                if (null != droite) {
                    resultat = droite.chercher(clef);
                } else {
                    resultat = null;
                }
            }
        } 
        return resultat;
    }

    /**
     * Parent | courant | resultat
     * -------+---------+---------
     * null   | null    | arbre est vide
     * null   | ! null  | racine contient la clef
     * ! null | null    | clef pas dans l'arbre
     * ! null | ! null  | clef est dans l'arbre
     */
    private Pair<Noeud<K>, Noeud<K>> chercherParentCourant (K clef) {
        Noeud<K> courant = racine;
        Noeud<K> parent = null;
 
        int direction = (null == courant) ? 0 : clef.compareTo(courant.clef) ;
 
        while (null != courant && direction != 0)  {
            if (direction < 0) {
                parent = courant;
                courant = courant.gauche;
            } else {
                parent = courant;
                courant = courant.droite;
            }
            direction = (null == courant) ? 0 : clef.compareTo(courant.clef);
 
        }
 
        return new Pair<> (parent,courant);
    }
    


    public boolean estVide() {
        return null == racine;
    }

}

```

### Class Pair <T1, T2>

Complémentaire à la classe ABR, nécessaire pour la méthode `chercherParentCourant()``

```java
public class Pair <T1, T2> {

    public T1 premier;
    public T2 deuxieme;
 
    public Pair (T1 p, T2 d) {
        premier = p;
        deuxieme = d;
    }
 
 }
 ```

 ## Suppression

 Dans un arbre binaire, il existe `3 cas` différents de suppression de donnée. 

 1. Supprimer une feuille
 2. Supprimer un élément ayant 1 enfant
        
        Revient à remplacer un noeud enfant par `null`.
        Si enfant possède un enfant. 

 3. Supprimer un élément ayant 2 enfants
    
        REMPLACER -> Précédant

Principe dud sous-arbre : Trouver un Maxima
- Pour trouver un maxima il faut se déplacer constamment vers la `droite`.


# Stream

Possibilité de travailler avec du multi coeur. Utilise la mécanique de `Thread` en JAVA.
Un stream est une façon de monter une chaîne de montage pour d'autres informations.

Le stream représente le tapis roulant d'une chaîne de montage. On y ajoute des éléments pour pouvoir démarrer la chaîne. 
Lors de la `construction` du stream, il n'y a rien qui se passe au tout début.

Pour constuire un stream, il est nécessaire d'avoir quelqu'un (quelque chose) qui envoie de l'information sur le tapis roulant
- **SUPPLIER** en Java.

Par la suite, lorsque tous les éléments auront traversés la chaîne de montage, il est nécessaire d'avoir une boîte.
- **CONSUMER** en Java.

### Map

Robot qui transforme l'information sur un stream. `Map` ajoute une fonction.

### Filter

Comparable à une caméra sur le tapis roulant. On ajoute un `PREDICATE` et donc il est possible de filtrer la chaîne de montage afin de conserver des éléments en particuliers.
- Comparable a un test de qualité


