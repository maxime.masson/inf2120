---
title : Cours 9 - JAVA II
author: [Maxime Masson]
titlepage : true
---

# Resume
- TDA
- LIST
    - FILE
    - PILE
- LIBRARY
- HERITAGE
    - Extends arraylist
- Linkedlist
    - Class Link

<br>

# Introduction

Review methods for Stack structure

```java

public class Pile2<E> {
    protected class Link<T> {
        protected E element;
        protected Link<T> previous;

        public Link(E element) {
            this.element = element;
            previous = null;
        }
        public Link(E element, Link<T> previous) {
            this.element = element;
            this.previous = previous;
        }
        public getElement() {
            return this.element;
        }
        public void setPrevious(Link<T> previous) {
            this.previous = previous;
        }
        public getPrevious() {
            return this.previous;
        }
    }

    protected Link<E> top;
    protected int size;

    public Pile2() {
        top = null;
    }

    public boolean isEmpty() {
        return null == top;
    }

    public E getTop() throws ExceptionPileIsEmpty {
        if (null == top) {
            throw new ExceptionPileIsEmpty("getSomme");
        }
        return top.getElement();
    }

    public void push(E element) {
        top = new Link(element, top);
        size++;
    }

    public E pop() throws ExceptionPileIsEmpty {
        E value = getTop(); // Method is already testing if the stack is empty
        top = top.getPrevious();
        size--;
        return value;
    }

    public int getSize() {
        return size;
    }
    public int size() {
        // Iterate through the stack
        int s = 0;
        Link<E> current = top;
        while (null != current) {
            current = current.getPrevious();
            s++;
        }
        return s;
    }
}

```

## Algorithm

Description : How is the mathematical function working ?
- Is most of the time operationnal.
- Pseudo code
    - for, while, if
    - expressions (math. set. etc.)
    - notations (infinity, etc.)
- Syntax is not formal
- Indentation

### What is the purpose ?

Algorithm are used to discuss about the solution before jumping into the coding.

It will help to find problematic methods where there's complexity and special usage.

### Ressources

- Temporal ressource

    The time it's taking to execute.

- Spatial ressource

    The complexity of the algorithm, memory space.

- Abstraction to predict time/space

    growth rate (Delta) of the function

- Classing functions by types

        f(n) is element of O( g(n) )
        f(n) grow slower or as fast as g(n)

        S'il existe s > 0 et k > 0, tel que x > s, F(x) <= K * G(x)

        For F(x) to be part of the same family than G(x), F(x) need to always be under or equal than G(x) in complexity.

        Exception : If the mathematical logic is false, then the function can't be in the same family as the other.

    Polynomial family
    - O(1) : constant
    - O(log n) : Logarithmique (n * 2 -> + 0.1 time)
    - O( n ) : Linear
    - O( n log n ) : no name
    - O( n^2 ) : Quadratique
    - O( n^3 ) : Cubic

    Exponential
    - O( 2^n ) : Terrible (n + 1 -> time x 2)
    - O( n^n ) : O( n! ) = Nop