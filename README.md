# PROGRAMMATION II - Java

## Description du cours

Approfondir les concepts de la programmation orientée-objet. Approfondir les concepts de mise au point et de test de
composants logiciels. Identification et définition des classes d'une solution logicielle. Relations entre les classes: composition et
héritage. Classes abstraites et polymorphisme. Introduction à la notation UML. Algorithmes récursifs simples. Structures de
données classiques: piles, files, listes et arbres binaires de recherche. Techniques classiques de recherche (séquentielle et
binaire) et de tri. Introduction à la programmation des interfaces graphiques (GUI). Gestion des événements et des exceptions.
Conception de paquetages. Introduction aux outils automatisés de validation. 

## Objectifs du cours

- Approfondir les méthodes de conception et de programmation orientée-objet
- Spécialisation des classes
- Connaître les structures de données fondamentales et savoir les choisir et les utiliser
- Connaître les techniques de base de la recherche de données et des tris
- Connaître la conception d'une interface graphique (GUI) avec les composants de base
- Acquérir des connaissances de base en génie logiciel
- S'initier aux étapes de la réalisation d'un produit logiciel
    - Stratégies de conception, de mise au point et de tests
    - Documentation du logiciel
- Approfondir le langage Java
- À la fin de la session, l'étudiant(e) devrait être en mesure d'élaborer un programme structuré et fonctionnel en utilisant
- les notions de génie logiciel étudiées au cours et en se servant des différentes structures de données fondamentales.

## Sujets abordés 

1. Classes
2. Paquetage
3. Héritage & Pointeur
4. Classe `Objet` et ses méthodes
5. Classe `Abstract` & `Interface` 
6. Les ArrayList
7. Les types de données abstrait (`TDA`)
    - TDA Pile
    - TDA File
8. Listes chainées
9. Les interfaces graphiques
10. Les techniques de recherche (Recherche binaire)
11. Arbres binaires
12. Algorithme de tris

## Énoncés des démos

Les énoncés de démonstration (Laboratoire de pratique) se trouve à la racine du répertoire.

Les `solutions` sont disponible sur le site de [Bruno Malenfant](http://www.labunix.uqam.ca/~malenfant_b/inf2120/inf2120.html).

