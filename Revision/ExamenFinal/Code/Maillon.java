public class Maillon<T> {
    private T info;
    private Maillon<T> suiv;

    public Maillon(T o) {
        this (o, null);
    }

    public Maillon(T o, Maillon<T> suivant) {
        info = o;
        suiv = suivant;
    }

    public T info() {
        return info;
    }
    public Maillon<T> suivant() {
        return suiv;
    }
    public void modifierInfo(T o) {
        info = o;
    }
    public void modifierSuivant(Maillon<T> suivant) {
        suiv = suivant;
    }
}