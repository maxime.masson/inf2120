public class Main {
    public static void main(String[] args) {
        Maillon<Integer> p = new Maillon<Integer> (3);
        Maillon<Integer> w = new Maillon<Integer> (4, p);
        p = new Maillon<Integer>(5,w);
        w = new Maillon<Integer>(6,p.suivant());
        p.modifierSuivant(w);
        while ( p != null ) {
            System.out.println(p.info());
            p = p.suivant();
        }
    }
}