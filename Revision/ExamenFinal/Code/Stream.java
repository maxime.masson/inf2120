import java.util.*;

public class Stream {
    public static void main(String[] args) {
        String[] names = {"Max", "Jeff", "Kaka", "Ced", "Fred", "John"};
        Arrays.asList(names).stream()
                                    .filter(x -> x.startsWith("M") || x.startsWith("C"))
                                    .sorted()
                                    .forEach(x -> System.out.print(x + " "));
                                    System.out.println("\n");
        new Random().ints(20, 0, 21)
                    .map(x -> x < 10 ? x = 0 : x)
                    .sorted()
                    .forEach(System.out::println);
    }
}