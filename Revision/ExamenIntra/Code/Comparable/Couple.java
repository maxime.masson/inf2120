public class Couple<T extends Comparable> implements Comparable<Couple<T>> {

    private T a1;
    private T a2;

    public Couple(T v1, T v2) {
        a1 = v1;
        a2 = v2;
    }

    @Override
    public Comparable.Ordre comparer(Couple<T> v2) {
        Comparable.Ordre r = this.a1.comparer(v2.a1);
        if (r == Comparable.Ordre.EGAL) {
            r = this.a2.comparer(v2.a2);
        }
        return r;
    }

    public static void main(String[] args) {
        Couple<Person> c1 = new Couple<>(new Person(21), new Person(23));
        Couple<Person> c2 = new Couple<>(new Person(21), new Person(23));
        System.out.println(c1.comparer(c2));
    }

}