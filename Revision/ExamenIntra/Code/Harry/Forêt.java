public  abstract class Forêt extends Animaux{
	protected Enum monEnum; 

	public Forêt(boolean etat , boolean aggressif,int Cenum) {
		super(etat,aggressif,Cenum) ;
		
	}
	public enum temps {Pluvieux,Humide,Sec} ;
	
	protected Enum quelEstLeTemps (int Cenum) {
		int temps0,temps1,temps2 ; 
		temps0 = temps.Pluvieux.ordinal();
		temps1 = temps.Humide.ordinal(); 
		temps2 = temps.Sec.ordinal(); 
		
		return monEnum = Cenum == temps1 ? temps.Humide : 
			( Cenum == temps2  ? temps.Sec :
				temps.Pluvieux );
		
	}
	
	public Forêt() {} ;
	
public String environnement() {
	return  "              _{\\ _{\\{\\/}/}/}__\n" + 
			"            {/{/\\}{/{/\\}(\\}{/\\} _\n" + 
			"           {/{/\\}{/{/\\}(_)\\}{/{/\\}  _\n" + 
			"        {\\{/(\\}\\}{/{/\\}\\}{/){/\\}\\} /\\}\n" + 
			"       {/{/(_)/}{\\{/)\\}{\\(_){/}/}/}/}\n" + 
			"      _{\\{/{/{\\{/{/(_)/}/}/}{\\(/}/}/}\n" + 
			"     {/{/{\\{\\{\\(/}{\\{\\/}/}{\\}(_){\\/}\\}\n" + 
			"     _{\\{/{\\{/(_)\\}/}{/{/{/\\}\\})\\}{/\\}\n" + 
			"    {/{/{\\{\\(/}{/{\\{\\{\\/})/}{\\(_)/}/}\\}\n" + 
			"     {\\{\\/}(_){\\{\\{\\/}/}(_){\\/}{\\/}/})/}\n" + 
			"      {/{\\{\\/}{/{\\{\\{\\/}/}{\\{\\/}/}\\}(_)\n" + 
			"     {/{\\{\\/}{/){\\{\\{\\/}/}{\\{\\(/}/}\\}/}\n" + 
			"      {/{\\{\\/}(_){\\{\\{\\(/}/}{\\(_)/}/}\\}\n" + 
			"        {/({/{\\{/{\\{\\/}(_){\\/}/}\\}/}(\\}\n" + 
			"         (_){/{\\/}{\\{\\/}/}{\\{\\)/}/}(_)\n" + 
			"           {/{/{\\{\\/}{/{\\{\\{\\(_)/}\n" + 
			"            {/{\\{\\{\\/}/}{\\{\\\\}/}\n" + 
			"             {){/ {\\/}{\\/} \\}\\}\n" + 
			"             (_)  \\.-'.-/\n" + 
			"         __...--- |'-.-'| --...__\n" + 
			"  _...--\"   .-'   |'-.-'|  ' -.  \"\"--..__\n" + 
			"-\"    ' .  . '    |.'-._| '  . .  '   jro\n" + 
			".  '-  '    .--'  | '-.'|    .  '  . '\n" + 
			"         ' ..     |'-_.-|\n" + 
			" .  '  .       _.-|-._ -|-._  .  '  .\n" + 
			"             .'   |'- .-|   '.\n" + 
			" ..-'   ' .  '.   `-._.-´   .'  '  - .\n" + 
			"  .-' '        '-._______.-'     '  .\n" + 
			"       .      ~,\n" + 
			"   .       .   |\\   .    ' '-.\n" + 
			"   ___________/  \\____________\n" + 
			"  /  Why is it, when you want \\\n" + 
			" |  something, it is so damn   |\n" + 
			" |    much work to get it?     |\n" + 
			"  \\___________________________/";

	
 }

}