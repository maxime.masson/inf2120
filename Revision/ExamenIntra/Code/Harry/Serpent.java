public class Serpent extends Forêt {
	private boolean venimeux ;

	public Serpent(boolean etat,boolean aggressivite,int Cenum) {
		super(etat,aggressivite,Cenum) ;
		setmonEnum((quelEstLeTemps(Cenum)));
		this.venimeux = false ;
	}
	
	public Serpent(boolean etat,boolean aggressivite,int Cenum,boolean venimeux) {
		this(etat,aggressivite,Cenum);
		this.venimeux = venimeux ;
	}
	
	
	public String chant () {
		return "taki taki, taki taki RUMBA  !! ";
	}
	
	public String toString () {
		String result;
		if(venimeux == false) {
			result =   super.toString() + " Toutefois je ne suis pas venimeux. \n" +environnement() ;
		}else
			result = super.toString() + " Toutefois faites attention je suis venimeux.\nSi je te mords, tu souffre ^^ \n " + environnement(); ;
		
		return result ;
	}
	
	@Override
	public boolean equals (Object serpent) {
		boolean result ;
		Serpent temp ;
		if(super.equals(serpent)==true) {
			temp = (Serpent)serpent; 
			result = this.venimeux == temp.venimeux ? true : false;
		}else
			result = false ;
			
			
			return result;
	}

}