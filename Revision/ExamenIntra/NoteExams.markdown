#Intra

*Question 6* 
Ne sera pas à l'examen

*Recrusion*
Ne sera pas à l'examen

```
6 à 10 question
1 question à l'écrit
1 question d'analyse
Reste des questions = lire/écrire du code
```

## À savoir pour l'examen
```java
add()
remove()
get()
set()
size()
isEmpty()
contains()

Class
Héritage
Abstract
Extends
Variable de type (type générique à notation diamand <>)
Interfaces (Implements, default)
instanceOF
ArrayList
Class Object (**Signature de equals**)
Comparable
Iterable
Iterator (.next(), .hasNext(), for avancé, fonction )
```

## Théorique
```
Reference (Pointeur)
Modele memoire (Type static, dynamique)
Parametrisation
```



