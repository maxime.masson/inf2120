

/**
 * Contient l'information pour modifier l'acces a un tableau.
 *
 * Cette classe decrit un nouveau mode d'acces a un tableau.  Ce nouveau mode
 * est construit en modifiant la position de la case identifiee par l'indice zero
 * et en modifiant le nombre d'elements accessible.
 *
 * @see CoupeA
 * @see CoupeDe
 * @see CoupeDeA
 * @see TableauPartiel
 */
public abstract class Coupe {

    /**
     * 
     * @return Retourne zero afin de definir l'index de debut du tableau
     */
    public int getDebut() {
        return 0;
    }

    /**
     * 
     * @return Retourne -1 dans l'eventualite qu'aucun index de fin n'est definit.
     * Indique donc que l'index de fin est la limite du tableau
     */
    public int getFin() {
        return -1;
    }
    
}
