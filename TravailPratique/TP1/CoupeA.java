

/**
 * Decrit une coupe.
 * Cette coupe ne modifie pas la position de la case zero mais
 * modifie la taille du tableau accede.
 *
 * @see Coupe
 * @see CoupeDe
 * @see CoupeDeA
 * @see TableauPartiel */
public class CoupeA extends Coupe {
    protected int fin;

    /**
     * Construit une nouvelle coupe.
     *
     * Cette coupe commence au debut du tableau et termine
     * a l'indice <code>fin - 1</code>.
     * @param fin l'indice de la derniere case du tableau (exclusive).
     *            Cette valeur doit etre plus grande que 0 et plus petite
     *            ou egal a la taille du tableau.
     */
    public CoupeA( int fin ){
        this.fin = fin;
    }

    /**
     * Retourne l'indice de fin du tableau.
     * @return l'indice de la case apres la derniere case du tableau.
     */
    public int getFin(){
        return fin;
    }

}
