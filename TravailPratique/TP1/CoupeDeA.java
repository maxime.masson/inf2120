

/**
 * Decrit une coupe.
 * Cette coupe indique une nouvelle position de la case zero et
 * modifie la taille du tableau accede.
 *
 * @see CoupeA
 * @see CoupeDe
 * @see Coupe
 * @see TableauPartiel */
public class CoupeDeA extends Coupe {
    protected int debut;
    protected int fin;

    /**
     *
     * Construit une nouvelle coupe.
     *
     * Cette coupe commence a l'indice <code>debut</code> et termine
     * a l'indice <code>fin - 1</code>.
     * @param debut l'indice de la premiere case du tableau (inclusive).
     *              Cette valeur doit etre plus grande que 0 et
     *              plus petite que <code>fin</code>.
     * @param fin l'indice de la derniere case du tableau (exclusive).
     *            Cette valeur doit etre plus grande que <code>debut</code> et plus
     *            petite ou egal a la taille du tableau.
     */
    public CoupeDeA( int debut, int fin ){
        this.debut = debut;
        this.fin = fin;
    }

    /**
     * Retourne l'indice de debut du tableau.
     * @return l'indice de la premiere case relative du tableau.
     */
    public int getDebut(){
        return debut;
    }

    /**
     * Retourne l'indice de fin du tableau.
     * @return l'indice de la case apres la derniere case du tableau.
     */
    public int getFin(){
        return fin;
    }
}
