import java.util.ArrayList;

/**
 * Les Objectifs du TP1
 *
 * -- Référence
 * -- Générique
 * -- Héritage
 * -- ArrayList
 * -- Module
 * -- JavaDocs
 *      1. Phrase qui dit ce que ça fait en gros
 *      2. Liste des paramètres avec identification
 *      3. Expliquer comment se servir de la méthode
 *      4. Expliquer ce que la méthode retourne
 * -- Comment Commenter
 *      1. @param
 *      2. @throws
 *      3. <code>Permet d'identifier une classe, type, valeur spéciale/précise</code>
 *
 * Tableau partiel : Tableau qu'on peut couper, est un <ArrayList>
 * -- Comprend les méthodes Get & Set
 * -- Comprend la référence vers le tableau
 *
 * Classes et Méthodes peuvent être créées
 * InstanceOf ne devrait pas être utilisé
 *
 *
 * Classe Coupe : Classe qui décrit comment couper un tableau. Elle sera Abstract
 *
 * Classe CoupeDeA : Coupe qui a un point de départ inclusif et un point de d'arrivé exclusif
 *
 * Classe CoupeA : Coupe qui part du début du tableau jusqu'a l'index A
 *
 * Classe CoupeDe : Coupe qui part d'un certain index jusqu'à la fin.
 *
 */
public class Main {
    public static final Integer HUIT     = new Integer(  8 );
    public static final Integer SEPT     = new Integer(  7 );
    public static final Integer SIX      = new Integer(  6 );
    public static final Integer CINQ     = new Integer(  5 );
    public static final Integer QUATRE   = new Integer(  4 );
    public static final Integer TROIS    = new Integer(  3 );
    public static final Integer DEUX     = new Integer(  2 );
    public static final Integer UN       = new Integer(  1 );
    public static final Integer M_UN     = new Integer( -1 );
    public static final Integer M_DEUX   = new Integer( -2 );
    public static final Integer M_TROIS  = new Integer( -3 );
    public static final Integer M_QUATRE = new Integer( -4 );
    public static final Integer M_CINQ   = new Integer( -5 );
    public static final Integer M_SIX    = new Integer( -6 );

    public static void main(String[] params) {
        Integer tab[] = { HUIT, SEPT, SIX, CINQ };
        TableauPartiel< Integer > tp = new TableauPartiel<>( tab );


        System.out.println(tp.estVide() + " doit donner false"); // False
        System.out.println(tp.get(1) + " doit donner 7"); // 7
        System.out.println(tp.taille() + " doit donner 4"); // 4

        tp.set(1, M_UN);
        System.out.println(tp.get(1) + " doit donner -1"); // -1
        System.out.println(tab[1] + " doit donner 7"); // 7

        try {
            TableauPartiel< Integer > tp_e = tp.coupe( new CoupeDeA( 2, 5 ) );
            System.out.println("Erreur ligne 68");			
		} catch( IndexOutOfBoundsException e ) {
			System.out.println("Erreur d'index bien lancé");
        }

        try {
			tp.position( UN );
			System.out.println(tp.position(UN) + " WAIT ERROR LIGNE 75");		
		} catch( ElementNonPresentException e ) {
			System.out.println("Element non present bien lancé");
		}
        
        // coupeDeA( 1, 3 );
        TableauPartiel< Integer > tp_c1 = tp.coupe( new CoupeDeA( 1, 3 ) );
        
        System.out.println(tp_c1.taille() + " doit donner 2"); // 2
        System.out.println(tp_c1.get(1) + " doit donner 6"); // 6

        tp_c1.set(1, M_DEUX); // Élément de la coupe ne modifie pas le tableau d'origine
        System.out.println(tp_c1.get(1) + " doit donner -2"); // -2
        System.out.println(tp.get(2) + " doit donner -2"); // -2 
        System.out.println(tab[2] + " doit donner 6"); // 6
        

        try {
			System.out.println(tp_c1.get( 2 ) + " Error ligne 95");
		} catch( IndexOutOfBoundsException e ) {
			System.out.println("Erreur d'index bien lancé");
        }
        
        try {
            tp_c1.set( 2, UN );	
            System.out.println("Error ligne 102");
		} catch( IndexOutOfBoundsException e ) {
			System.out.println("Erreur d'index bien lancé");
        }	
        

        // coupe( coupeDe );
        TableauPartiel< Integer > tp_c2 = tp.coupe( new CoupeDe( 1 ) );
        System.out.println(tp_c2.taille() + " doit donner 3"); // 3
        System.out.println(tp_c2.get(2) + " doit donner 5"); // 5

        tp_c2.set(2, M_TROIS);
        System.out.println(tp_c2.get(2) + " doit donner -3"); // -3
        System.out.println(tp.get(3) + " doit donner -3"); // -3 
        System.out.println(tab[3] + " doit donner 5"); // 5

        // 2iem constructeur.
        TableauPartiel< Integer > tp2 = new TableauPartiel<>( tp_c2 );
        System.out.println(tp2.taille() + " doit donner 3"); // 3
        System.out.println(tp2.get(2) + " doit donner -3"); // -3

        tp2.set(0, M_QUATRE);
        System.out.println(tp2.get(0) + " doit donner -4"); // -4
        System.out.println(tp_c2.get(0) + " doit donner -1"); // -1

        // *******************************************************
        // section 2
        
        Integer tab2[] = { HUIT, SEPT, SIX, SEPT };
        TableauPartiel< Integer > tp3 = new TableauPartiel<>( tab2 );

        System.out.println(tp3.contient(SIX) + " doit donner true");
        System.out.println(tp3.contient(CINQ)+ " doit donner false");

        try {
            System.out.println(tp3.position(SIX) + " doit donner 2");
		} catch ( ElementNonPresentException e ) {
            System.out.println("Ne devrait pas lancer cette exception");;
        }
        
        try {
			System.out.println(tp3.get( 8 ) + " ERREUR");
		} catch( IndexOutOfBoundsException e ) {
			System.out.println("Exception lancé comme prévue");
        }
        
        try {
            tp3.set( 8, UN );
            System.out.println("tp3.set(8, UN) Error ligne 151");
		} catch( IndexOutOfBoundsException e ) {
            System.out.println("IndexOutOfBound bien lancé");
			// rien
		}
        try {
            tp3.remplacer( SEPT, M_CINQ );
        } catch (ElementNonPresentException e) {
            System.out.println("Erreur d'element pas sensé arriver");
        }
        
        System.out.println("tp3.remplacer(7, -5) effectué");
        System.out.println(tp3.get(0) + " doit donner 8");
        System.out.println(tp3.get(1) + " doit donner -5");
        System.out.println(tp3.get(2) + " doit donner 6");
        System.out.println(tp3.get(3) + " doit donner -5");
        TableauPartiel< Integer > tp_c3 = tp3.coupe( new CoupeA( 0 ) );

        System.out.println(tp_c3.taille() + " doit donner 0");
        System.out.println(tp_c3.estVide() + " doit donner true");

    }



}
