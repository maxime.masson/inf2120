import static org.junit.Assert.*;


public class TableauPartielTestM {
	public static final Integer HUIT     = new Integer(  8 ); 
	public static final Integer SEPT     = new Integer(  7 ); 
	public static final Integer SIX      = new Integer(  6 ); 
	public static final Integer CINQ     = new Integer(  5 ); 
	public static final Integer QUATRE   = new Integer(  4 ); 
	public static final Integer TROIS    = new Integer(  3 ); 
	public static final Integer DEUX     = new Integer(  2 ); 
	public static final Integer UN       = new Integer(  1 ); 
	public static final Integer M_UN     = new Integer( -1 ); 
	public static final Integer M_DEUX   = new Integer( -2 ); 
	public static final Integer M_TROIS  = new Integer( -3 ); 
	public static final Integer M_QUATRE = new Integer( -4 ); 
	public static final Integer M_CINQ   = new Integer( -5 ); 
	public static final Integer M_SIX    = new Integer( -6 ); 
	
	public static void main( String [] args ) {
		Integer tab[] = { HUIT, SEPT, SIX, CINQ };
		TableauPartiel< Integer > tp = new TableauPartiel<>( tab );
		
		assertFalse( tp.estVide() );
		assertEquals( SEPT, tp.get( 1 ) );
		assertEquals( 4, tp.taille() );

		tp.set( 1, M_UN );
		assertEquals( M_UN, tp.get( 1 ) );
		assertEquals( SEPT, tab[1] );

		try {
			TableauPartiel< Integer > tp_e = tp.coupe( new CoupeDeA( 2, 5 ) );
			fail( "devrait etre une exception." );				
		} catch( IndexOutOfBoundsException e ) {
			// rien
		}
		
		try {
			tp.position( UN );
			fail( "devrait etre une exception." );			
		} catch( ElementNonPresentException e ) {
			// rien
		}
		
		// coupeDeA( 1, 3 );
		TableauPartiel< Integer > tp_c1 = tp.coupe( new CoupeDeA( 1, 3 ) );

		assertEquals( 2, tp_c1.taille() );
		assertEquals( SIX, tp_c1.get( 1 ) );

		tp_c1.set( 1, M_DEUX );
		assertEquals( M_DEUX, tp_c1.get( 1 ) );
		assertEquals( M_DEUX, tp.get( 2 ) );
		assertEquals( SIX, tab[2] );
		
		try {
			tp_c1.get( 2 );
			fail( "devrait etre une exception." );	
		} catch( IndexOutOfBoundsException e ) {
			// rien
		}

		try {
			tp_c1.set( 2, UN );
			fail( "devrait etre une exception." );	
		} catch( IndexOutOfBoundsException e ) {
			// rien
		}		
		
		// coupe( coupeDe );
		TableauPartiel< Integer > tp_c2 = tp.coupe( new CoupeDe( 1 ) );
		assertEquals( 3, tp_c2.taille() );
		assertEquals( CINQ, tp_c2.get( 2 ) );
		
		tp_c2.set( 2, M_TROIS );
		assertEquals( M_TROIS, tp_c2.get( 2 ) );
		assertEquals( M_TROIS, tp.get( 3 ) );
		assertEquals( CINQ, tab[3] );
				
		// 2iem constructeur.
		TableauPartiel< Integer > tp2 = new TableauPartiel<>( tp_c2 );
		assertEquals( 3, tp2.taille() );
		assertEquals( M_TROIS, tp2.get( 2 ) );
		
		tp2.set( 0, M_QUATRE );
		assertEquals( M_QUATRE, tp2.get( 0 ) );
		assertEquals( M_UN, tp_c2.get( 0 ) );

		// *******************************************************
		// section 2

		Integer tab2[] = { HUIT, SEPT, SIX, SEPT };
		TableauPartiel< Integer > tp3 = new TableauPartiel<>( tab2 );
		
		assertTrue( tp3.contient( SIX ) );
		assertFalse( tp3.contient( CINQ ) );
		try {
			assertEquals( 2, tp3.position( SIX ) );
		} catch ( ElementNonPresentException e ) {
			fail( "Ne devrait pas lancer d'exception." );
		}
		
		try {
			tp3.get( 8 );
			fail( "devrait etre une exception." );
		} catch( IndexOutOfBoundsException e ) {
			// rien
		}
		try {
			tp3.set( 8, UN );
			fail( "devrait etre une exception." );
		} catch( IndexOutOfBoundsException e ) {
			// rien
		}
		
		tp3.remplacer( SEPT, M_CINQ );
		assertEquals( HUIT, tp3.get( 0 ) );
		assertEquals( M_CINQ, tp3.get( 1 ) );
		assertEquals( SIX, tp3.get( 2 ) );
		assertEquals( M_CINQ, tp3.get( 3 ) );
	
		// coupe( coupeA );
		TableauPartiel< Integer > tp_c3 = tp3.coupe( new CoupeA( 0 ) );
		assertEquals( 0, tp_c3.taille() );
		assertTrue( tp_c3.estVide() );		
	}
}