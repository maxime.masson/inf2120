class testClass implements in1 {
    public static void main(String[] params) {
        testClass t = new testClass();
        t.display();
    }
}

// It is possible to add  DEFAULT IMPLEMENTATION to interface methods
/**
 * default void methods() {}
 * t.display will print hello
 */

// It is possible to define STATIC METHODS in interface
// They can be call WITHOUT any OBJECT
/**
 * in1.display() will print Hello
 */